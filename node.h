#ifndef NODE_H
#define NODE_H

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

/* Códigos de retorno */
enum {
    success   = 0, /* operação realizada com sucesso. */
    param_e   = 1, /* erro de parâmetro inválido.     */
    memory_e  = 2, /* erro de alocação de memória.    */
    unknown_e = 3, /* erro desconhecido.              */
};

typedef struct n {
    int       data;  /* informação armazenada pelo nó.    */
    struct n *next;  /* ponteiro para o próximo elemento. */
} Node;

/**
 * Cria uma novo nó.
 *
 * return Node* em caso de sucesso ou NULL caso contrário.
 */
Node* node_create(int data);

/**
 * Destroi um nó.
 *
 * param [in] v - nó a ser desalocado.
 */
void  node_destroy(Node* n);

#endif