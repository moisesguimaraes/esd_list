#!/bin/bash

rm a.out
gcc node.c list.c main.c

echo ">>> INSERÇÃO <<<"

echo "Criando lista [1, 3, 6, 9, 12, 15]"
./a.out 0 1 1 3 2 6 3 9 4 12 5 15 print

echo "Criando lista [0, 5, 1, 4, 2, 3]"
./a.out 5 5 4 4 3 3 2 2 1 1 0 0 print

echo "Criando lista [0, 1, 2, 3, 4, 5]"
./a.out 0 5 0 4 0 3 0 2 0 1 0 0 print

echo ">>> REMOÇÃO <<<"

echo "Removendo de lista vazia"
./a.out -0 -1 -2 -3 print

echo "Removendo elementos primos de [1..10]"
./a.out 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 print -7 -5 -3 -2 print

echo "Removendo tudo!"
./a.out 0 0 1 1 2 2 3 3 4 4 5 5 print -5 -4 -3 -2 -1 -0 -3 print 0 0 3 3 print

echo ">>> BUSCA <<<"

echo "Buscando 7"
./a.out 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 print f 7

echo "Contando 7s"
./a.out 0 0 1 1 2 7 3 7 4 4 5 7 6 6 7 7 8 8 9 9 10 10 print c 7

echo "Elemento inexistente"
./a.out 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 print f 100 c 100
