#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "list.h"

void printl(List* l)
{
    Node* n;
    
    printf("l -> ");
    
    for (n = l->head; n; n = n->next)
        printf("[%d]%s", n->data, n->next ? "->" : "¬");
    
    printf("\n");
}

void printe(Node* n)
{
    if (n)
        printf("[%d]%s", n->data, n->next ? "->#\n" : "¬\n");
}

void helper(void) {
    printf("uso: PROG [argumentos]\n"
        "\nonde os argumentos são:"
        "\n\t i n   : Insere o número 'n' na posição 'i' da lista."
        "\n\t-i     : Remove o elemento na posição 'i' da lista."
        "\n\t f n   : Busca o primeiro 'n' na lista o imprime."
        "\n\t c n   : Imprime a quantidade de ocorrências de 'n' na lista."
        "\n\t r     : Inverte a ordem dos elementos na lista."
        "\n\t print : Imprime a lista."
        "\n");
}

int main(int argc, char** argv)
{
    int index, i, n;
    List *l = list_create();
    
    if(argc < 2)
        helper();
    
    if (l == NULL)
        return memory_e;
    
    for (index = 1; index < argc; index++) {
        switch (argv[index][0]) {
            case 'f':
                printe(list_find(l, atoi(argv[++index])));
            break;
        
            case 'c':
                printf("%d\n", list_count(l, atoi(argv[++index])));
            break;
            
            case 'p':
                if (!strcmp(argv[index], "print"))
                    printl(l);
            break;
        
            case '-':
                list_remove(l, -atoi(argv[index]));
            break;
            
            case 'r':
                list_reverse(l);
            break;
            
            default:
                list_insert(l, atoi(argv[index]), atoi(argv[index+1]));
                index++;
            break;    
        }
    }
    
    list_destroy(l);
    
    return success;
}
